var app = app || {};

/*
LEADER MODEL ATTRIBUTES:
- ranking
- name
- score
- picture
*/

app.LeaderModel = Backbone.Model.extend({
	defaults: {
		name: 'N/A', 
		picture: 'default.jpg',
	},
		
});