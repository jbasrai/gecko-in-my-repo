var app = app || {};


$(document).ready(function() {
	
	var leaderList = new app.LeaderCollection();
	var leaderListView = new app.LeaderListView({collection:leaderList});
	
	$('.nav-tabs li').on('click', function() {
		leaderList.fetch({ 
			data : { interval : $(this).text() },
			reset: true,
		});
	});
	
	$($('.nav-tabs li')[0]).trigger('click');
	
});