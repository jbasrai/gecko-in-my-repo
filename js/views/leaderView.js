var app = app || {};

app.LeaderView = Backbone.View.extend({
	tagName: 'li',
	className: 'leader',
	template: _.template('<div class="ranking">#<%= ranking %></div> \
	<img src="assets/photos/<%=picture%>" class="picture"/> \
	<div class="name"><%= name %></div> \
	<div class="score"><%= score %>&nbsp;hour<% if(score > 1) %>s idle</div> \
	<div class="quote">"<%= quote %>"</div>'),
	render: function() {
		var html = this.template(this.model.attributes);
		this.$el.html(html);
	},
});