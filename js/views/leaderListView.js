var app = app || {};

app.LeaderListView = Backbone.View.extend({
	el: '#leaderboard',
	initialize: function() {
		this.collection.on('add', this.addOne, this);
		this.collection.on('reset', this.render, this);
	},
	render: function(collection) {
		this.$el.empty();
		collection.each(this.addOne, this);
	},
	
	addOne: function(leader) {
		//leader.set({quote:quotes.shift()});
		var leaderView = new app.LeaderView({model:leader});
		leaderView.render();
		this.$el.append(leaderView.el);
	},
});