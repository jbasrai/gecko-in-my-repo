var app = app || {};

app.LeaderCollection = Backbone.Collection.extend({
	url:'srv/get_rankings.php',
	model:app.LeaderModel,
	initialize: function() {
		this.on('reset', function() {
			tempQuotes = _.shuffle(quotes);
			this.each(function(leader) {
				tempQuote = tempQuotes.shift();
				this.assignQuote(leader, tempQuote)
			}, this);
		});
	},
	assignQuote: function(leader, quote) {
		leader.set({quote:quote});
	},
});

var quotes = [
	"Whatever the mind of man can conceive and believe, it can achieve.",
	"Your time is limited, so don't waste it living someone else's life.",
	"Strive not to be a success, but rather to be of value.",
	"Two roads diverged in a wood, and I took the one less traveled by, and that has made all the difference.",
	"The common question that gets asked in business is, 'why?' That's a good question, but an equally valid question is, 'why not?'",
	"You miss 100% of the shots you don't take.",
	"Every strike brings me closer to the next home run.",
	"Definiteness of purpose is the starting point of all achievement.",
	"Eighty percent of success is showing up.",
	"Life is what happens to you while you're busy making other plans.",	
];